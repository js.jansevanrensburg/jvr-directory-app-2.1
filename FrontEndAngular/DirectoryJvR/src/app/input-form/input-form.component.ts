import { Component, EventEmitter, Injectable, OnDestroy, OnInit, Output, ViewChild } from '@angular/core';
import { InputFormService } from './input-form.service';
import { Path } from './path.model';
import { Subject, Subscription } from 'rxjs';
import { NgForm } from '@angular/forms';

@Injectable({providedIn: 'root'})
@Component({
  selector: 'app-input-form',
  templateUrl: './input-form.component.html',
  styleUrls: ['./input-form.component.css']
})
export class InputFormComponent implements OnInit, OnDestroy {
  @ViewChild('f') piForm: NgForm;
  @Output() pathSubmitted = new EventEmitter<string>();

  postData: [{}];
  newPath: string;
  obsArr : [{}] = [{}];
  subscription : Subscription;
  id: number;
  public path :Path = {
    path: ""
};

  constructor(
    private inputFormService: InputFormService) { }

  ngOnInit() {
    this.subscription = (this.inputFormService.postReturn
    .subscribe(
    (data => {
     this.postData = data;
      data.forEach(data => {
      this.obsArr.push(data);
     })
    })
  ))
}

ngOnDestroy(): void {
  this.subscription.unsubscribe();
}


  onSubmitPath(form: NgForm){
    this.inputFormService.submitPath(form.value.path);
  }

  oneUp(path1){
    var parentFolder = path1.slice(0, path1.lastIndexOf("\\"));
    var parentFolder2 = parentFolder.slice(0, parentFolder.lastIndexOf("\\"));
    return(parentFolder2);
  }

  onBack(){ 
    const arr1 = (this.obsArr[this.obsArr.length-1]);
    const arr2 = arr1[0];
    this.inputFormService.submitPath(this.oneUp(arr2.path));
  }

  onClear(){  
    this.piForm.reset(); 
  }
  
}

