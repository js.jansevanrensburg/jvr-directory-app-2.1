import { Component, Input, OnInit } from '@angular/core';
import { InputFormService } from '../input-form.service';

@Component({
  selector: 'app-directory-item',
  templateUrl: './directory-item.component.html',
  styleUrls: ['./directory-item.component.css']
})
export class DirectoryItemComponent implements OnInit {

  @Input() directories: any;
  @Input() index: number;


  constructor( 
    private inputFormService: InputFormService) { }

  ngOnInit(){}

  onSelected(){
    if(this.directories.filedir === true){
    this.inputFormService.submitPath(this.directories.path);
    }
  }

}
