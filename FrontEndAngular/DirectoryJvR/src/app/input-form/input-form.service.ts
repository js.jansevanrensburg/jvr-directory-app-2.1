import {  Subject } from "rxjs";
import { HttpClient, } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Path } from "./path.model";

@Injectable({providedIn: 'root'})

export class InputFormService {

    postReturn = new Subject<any>();
    public path :Path = {
        path: ""
    };

    constructor(private http: HttpClient){}

    submitPath(inputPath: string){
        this.path.path = inputPath; 
        this.postReturn.next(this.http.post<any>('http://127.0.0.1:3000/ping', this.path))
      }

    }
 

