import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { InputFormComponent } from './input-form/input-form.component'

const appRoutes: Routes = [
    { path: '', redirectTo: '/input', pathMatch: 'full'},
    { path: 'input', component: InputFormComponent, children: [
        {path:':id', redirectTo: '/input'}
    ]},
   
]

@NgModule({
    imports : [RouterModule.forRoot(appRoutes)],
    exports : [RouterModule]
})

export class AppRoutingModule{}