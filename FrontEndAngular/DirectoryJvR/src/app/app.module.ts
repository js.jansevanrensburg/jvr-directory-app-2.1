import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { DirectoryItemComponent } from './input-form/directory-item/directory-item.component';
import { HeaderComponent } from './header/header.component';
import { InputFormService } from './input-form/input-form.service';
import { InputFormComponent } from './input-form/input-form.component';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    DirectoryItemComponent,
    InputFormComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [InputFormService, HeaderComponent, DirectoryItemComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
