const express = require('express')
const bodyParser = require('body-parser');;
const path = require('path');
const fs = require('fs');
const { promisify } = require("util");

const app = express()

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}) );

app.all("/*", function(req, res, next){
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  next();
});

app.post('/ping', function (req, res) {

   const errPath =   [{
    name: 'NO SUCH PATH',
    path: 'NO SUCH PATH',
    filedir: false,
    }];

  const pathSent = (req.body.path);

    const asyncStat = promisify(fs.stat);
  
    fs.readdir(pathSent, async function(err, files) {
      if (err) {
        res.send(errPath);
      } else
        {
          const data = await Promise.all(files.map(async function(file) {
            try {
             const stats = await asyncStat(pathSent +'//' + file);
              return { name: file, path: path.join(pathSent, file) , filedir: stats.isDirectory(), stats };
              } catch (error) {
              res.send(errPath);
            }
          }));
          res.send(data);
       }
    });
})

app.listen(3000, function () {
  console.log('App listening on port 3000')
})




